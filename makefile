question1-loop :
	go run main.go question1-loop

question1-without-loop :
	go run main.go question1-without-loop

question2 :
	go run main.go question2

question3 :
	go run main.go question3

api-run :
	go run main.go api

test-question1-loop :
	go test questions/question1/question1-solution-loop_test.go

test-question1-without-loop :
	go test questions/question1/question1-solution-without-loop_test.go

test-question2 :
	go test questions/question2/question2_test.go

test-question3 :
	go test questions/question3/question3_test.go

test : 
	go test questions/question1/question1-solution-loop_test.go
	go test questions/question1/question1-solution-without-loop_test.go
	go test questions/question2/question2_test.go
	go test questions/question3/question3_test.go
	go test -cover ./...