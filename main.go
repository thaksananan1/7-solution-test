package main

import (
	"TestQuestion/questions/question1"
	"TestQuestion/questions/question2"
	"TestQuestion/questions/question3"
	"TestQuestion/routes"
	"TestQuestion/utils"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
)

func main() {
	if len(os.Args) != 2 {
		fmt.Println("Usage: go run main.go <question>")
		return
	}

	questionName := os.Args[1]

	switch questionName {
	case "question1-loop":
		data, _ := utils.ReadJSON("files/numberList.json")
		fmt.Println(question1.TriangleSumValueWithLoop(data))

	case "question1-without-loop":
		data, _ := utils.ReadJSON("files/numberList.json")
		fmt.Println(question1.TriangleSumValueWithoutLoop(data))

	case "question2":
		var inputText string
		fmt.Printf("input : ")
		fmt.Scanln(&inputText)

		decoded := question2.DecodeString(inputText)
		fmt.Printf("input = '%s' output = '%s'\n", inputText, decoded)
	case "question3":
		router := gin.Default()
		router.GET("/beef/summary", func(c *gin.Context) {
			// Make an HTTP GET request to the API
			response, _ := http.Get("https://baconipsum.com/api/?type=meat-and-filler&paras=99&format=text")

			body, _ := ioutil.ReadAll(response.Body)
			text := string(body)

			c.JSON(http.StatusOK, gin.H{
				"beef": question3.PieFireDire(text),
			})

		})
		router.Run(":3000")

	case "api":
		router := gin.Default()
		routes.APIRoutes(router)
		router.Run(":3000")
	}

}
