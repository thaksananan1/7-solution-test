package question2

import "strconv"

func IncreaseLeftSide(decode, encode string) string {
	splitDecode := []rune(decode)
	splitDecodeLength := len(splitDecode)
	stepEncode := encode[:splitDecodeLength-1]
	RLastIndex := -1

	for i, char := range stepEncode {
		if char == 'R' {
			RLastIndex = i
		}
	}

	for i := splitDecodeLength - 1; i >= 0; i-- {
		if RLastIndex == i {
			value, _ := strconv.Atoi(string(splitDecode[i]))
			splitDecode[i+1] = []rune(strconv.Itoa(value + 2))[0]
			break
		}
		value, _ := strconv.Atoi(string(splitDecode[i]))
		splitDecode[i] = []rune(strconv.Itoa(value + 1))[0]
	}

	return string(splitDecode)
}

func DecodeString(encoded string) string {
	decode := "0"

	for _, char := range encoded {
		lastCharInDecode, _ := strconv.Atoi(string(decode[len(decode)-1]))
		if char == 'L' {
			decode = IncreaseLeftSide(decode, encoded)
			decode += "0"
		} else if char == 'R' {
			decode += strconv.Itoa(lastCharInDecode + 1)
		} else if char == '=' {
			decode += strconv.Itoa(lastCharInDecode)
		}
	}

	return decode
}
