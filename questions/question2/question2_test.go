package question2_test

import (
	"TestQuestion/questions/question2"
	"testing"
)

func TestQuestion2(t *testing.T) {
	IncreaseLeftSideDecode1 := "010"
	IncreaseLeftSideEncode1 := "RL"
	IncreaseLeftSideExpected1 := "021"
	result1 := question2.IncreaseLeftSide(IncreaseLeftSideDecode1, IncreaseLeftSideEncode1)
	if result1 != IncreaseLeftSideExpected1 {
		t.Errorf("Expected %s, but got %s for decode1 and encode1", IncreaseLeftSideExpected1, result1)
	}

	IncreaseLeftSideDecode2 := "0110"
	IncreaseLeftSideEncode2 := "RRL"
	IncreaseLeftSideExpected2 := "0131"
	result2 := question2.IncreaseLeftSide(IncreaseLeftSideDecode2, IncreaseLeftSideEncode2)
	if result2 != IncreaseLeftSideExpected2 {
		t.Errorf("Expected %s, but got %s for decode2 and encode2", IncreaseLeftSideExpected2, result2)
	}

	IncreaseLeftSideDecode3 := "210"
	IncreaseLeftSideEncode3 := "LL"
	IncreaseLeftSideExpected3 := "321"
	result3 := question2.IncreaseLeftSide(IncreaseLeftSideDecode3, IncreaseLeftSideEncode3)
	if result3 != IncreaseLeftSideExpected3 {
		t.Errorf("Expected %s, but got %s for decode3 and encode3", IncreaseLeftSideExpected3, result3)
	}

	encoded1 := "LLRR="
	expected1 := "210122"
	DecodeStringResult1 := question2.DecodeString(encoded1)
	if DecodeStringResult1 != expected1 {
		t.Errorf("Expected %s, but got %s for encoded1", expected1, DecodeStringResult1)
	}

	encoded2 := "==RLL"
	expected2 := "000210"
	DecodeStringResult2esult2 := question2.DecodeString(encoded2)
	if DecodeStringResult2esult2 != expected2 {
		t.Errorf("Expected %s, but got %s for encoded2", expected2, DecodeStringResult2esult2)
	}

	encoded3 := "=LLRR"
	expected3 := "221012"
	DecodeStringResult3 := question2.DecodeString(encoded3)
	if DecodeStringResult3 != expected3 {
		t.Errorf("Expected %s, but got %s for encoded3", expected3, DecodeStringResult3)
	}
}
