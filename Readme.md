# 7 Solution Test

Hello, I'm thaksa nanan, graduated from Mae Fah Luang University, majoring in Software Engineering. I would like to apply for a Backend Developer position.

### stack me:

| language | year                    |
| -------- | ----------------------- |
| NodeJs   | 2                       |
| Python   | 1                       |
| PHP      | 1                       |
| Java     | <1                      |
| go       | never but want to learn |

## Test Question ( backend-challenge )

https://github.com/7-solutions/backend-challenge

## Qustion1 : The most way binary Source tree

# How to run project

logic with loop

```go
make question1-loop
```

logic without loop ( Recursive function )

```go
make question1-without-loop
```

## Qustion2 : Catch me Left-Right-Equal

```go
make question2
```

## Qustion3 : Pie Fire Dire

```go
make question3
```

after you run project with question 3, it will API by Request API

method | URL |
--- | --- |
GET | http://localhost:3000/beef/summary

## Unit test

you can run unit testing with :

```go
# test all
make test

# test question1-loop
make test-question1-loop

# test question1-without-loop
make test-question1-without-loop

# test question2
make test-question2

# test question3
make test-question3
```

## Optional\*\*

### if you want to run every question will API

POSTMAN LINK : https://api.postman.com/collections/10147347-d79bf3ed-11e9-4805-bbd0-fecd8684bac4?access_key=PMAT-01HDHHK40M4VAV3FFK8GNV7MZS

## How to run

```go
make api-run
```


question | solution | method | URL | body
--- | --- | --- | --- | ---
1 | TriangleSumValueWithLoop | GET | http://localhost:3000/api/binary-tree/loop 
1 | TriangleSumValueWithoutLoop | GET | http://localhost:3000/api/binary-tree/without-loop
2 | DecodeString | POST | http://localhost:3000/api/catch-me | { "key": "LLRR="}
3 | PieFireDire | GET | http://localhost:3000/api/beef/summary

# Thank You
