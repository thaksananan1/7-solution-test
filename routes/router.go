package routes

import (
	"TestQuestion/questions/question1"
	"TestQuestion/questions/question2"
	"TestQuestion/questions/question3"
	"TestQuestion/utils"
	"io/ioutil"
	"net/http"

	"github.com/gin-gonic/gin"
)

// APIRoutes is an exported function to set up API routes.
func APIRoutes(router *gin.Engine) {
	api := router.Group("/api")

	question1GroupRoutes := api.Group("/binary-tree")
	question1GroupRoutes.GET("/loop", func(ctx *gin.Context) {
		data, _ := utils.ReadJSON("files/numberList.json")

		ctx.JSON(http.StatusOK, gin.H{
			"result": question1.TriangleSumValueWithLoop(data),
		})
	})
	question1GroupRoutes.GET("/without-loop", func(ctx *gin.Context) {
		data, _ := utils.ReadJSON("files/numberList.json")

		ctx.JSON(http.StatusOK, gin.H{
			"result": question1.TriangleSumValueWithoutLoop(data),
		})
	})

	api.POST("/catch-me/", func(ctx *gin.Context) {
		var requestBody struct {
			Key string `json:"key"`
		}

		if err := ctx.ShouldBindJSON(&requestBody); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}

		decoded := question2.DecodeString(requestBody.Key)

		ctx.JSON(http.StatusOK, gin.H{
			"result": decoded,
		})
	})

	api.GET("/beef/summary", func(ctx *gin.Context) {
		response, _ := http.Get("https://baconipsum.com/api/?type=meat-and-filler&paras=99&format=text")

		body, _ := ioutil.ReadAll(response.Body)
		text := string(body)

		ctx.JSON(http.StatusOK, gin.H{
			"beef": question3.PieFireDire(text),
		})
	})
}
